//
//  Test.swift
//  AppWeather
//
//  Created by Đỗ Hồng Quân on 25/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import UIKit

class Test: UIViewController {

    @IBOutlet weak var testt: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let urlString = "https://tile.openweathermap.org/map/temp_new/0/0/0.png?appid=0f17a750e711861498dc2d99cd03fa8d"
        let urlImage = URL(string: urlString)
        let data = try? Data(contentsOf: urlImage!)
        let a = UIImage(data: data!)
        testt.image = a
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
