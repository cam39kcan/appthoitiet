//
//  NetworkManager.swift
//  APIWeather
//
//  Created by Đỗ Hồng Quân on 14/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit

class NetworkManager {
    static let shared = NetworkManager()
    
    let baseURLString1 = "https://api.openweathermap.org/data/2.5/weather?id="
    let baseURLString2 = "https://api.openweathermap.org/data/2.5/forecast?id="
    let keyAPI = "&units=metric&lang=vi&APPID=0f17a750e711861498dc2d99cd03fa8d"
    func getWeatherAPIDecoder1 (_ id: Int, completion: @escaping (APIWeather?) -> Void) {
        let finalURLString = baseURLString1 + String(id) + keyAPI
        guard let url = URL(string: finalURLString)
            else{
                completion(nil)
                return
        }
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, response, error) in
            if let _ = error {
                completion(nil)
            }
            guard let data = data
                else{
                    completion(nil)
                    return
            }
            do {
                let weatherMap = try JSONDecoder().decode(APIWeather.self, from: data)
                completion(weatherMap)
            }
            catch {
                completion(nil)
            }
        }
        task.resume()
    }
    
    func getWeatherAPIDecoder2 (_ id: Int, completion: @escaping (APIWeather5days?) -> Void) {
        let finalURLString = baseURLString2 + String(id) + keyAPI
        guard let url = URL(string: finalURLString)
            else{
                completion(nil)
                return
        }
        let session = URLSession.shared
        let task = session.dataTask(with: url) { (data, response, error) in
            if let _ = error {
                completion(nil)
            }
            guard let data = data
                else{
                    completion(nil)
                    return
            }
            do {
                let weatherMap = try JSONDecoder().decode(APIWeather5days.self, from: data)
                print(weatherMap.list?.count)
                completion(weatherMap)
            }
            catch {
                completion(nil)
            }
        }
        task.resume()
    }
    
}
