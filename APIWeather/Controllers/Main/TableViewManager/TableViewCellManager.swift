//
//  TableViewCellManager.swift
//  AppWeather
//
//  Created by Đỗ Hồng Quân on 23/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import UIKit

class TableViewCellManager: UITableViewCell {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var temp: UILabel!
    @IBOutlet weak var date: UILabel!
    
    func setData(_ icon: UIImage?, _ time: String?, _ date: String?, _ temp: String?) {
        self.icon.image = icon
        self.time.text = time
        self.date.text = date
        self.temp.text = temp
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
