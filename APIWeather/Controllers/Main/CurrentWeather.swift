//
//  CurrentWeather.swift
//  AppWeather
//
//  Created by Đỗ Hồng Quân on 20/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import UIKit

class CurrentWeather: UIViewController {
/*  pressure
    humidity
    sea_level
    grnd_level
    wind.speed
    wind.deg
    sunrise
    sunset
 */
    
    
    // MARK: - Outlets
    @IBOutlet weak var tableView5days: UITableView!
    @IBOutlet weak var currentPlace: UILabel!
    @IBOutlet weak var currentDT: UILabel!
    @IBOutlet weak var iconWeather: UIImageView!
    @IBOutlet weak var descriptionWeather: UILabel!
    @IBOutlet weak var currentTemp: UILabel!
    @IBOutlet weak var temp_Max: UILabel!
    @IBOutlet weak var temp_Min: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var sunrise: UILabel!
    @IBOutlet weak var sunset: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableViewSearch: UITableView!
    @IBOutlet weak var btGetGPS: UIButton!
    
    
    // MARK: - variables
    var arrList = [List]()
    var arrList1 = [List]()
    var idReturn = 1581129
    var cityList = [CityList]()
    var currentCityList = [CityList]()
    
    
    // MARK: - View life cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        btGetGPS.layer.cornerRadius = btGetGPS.bounds.size.width / 2
        btGetGPS.clipsToBounds = true
        ReadJSON.shared.loadData(&cityList)
        currentReadAPIData()
        forecastReadAPIData()
        tableViewSearch.isHidden = true
        tableViewInit()
        setUpTableView()
        setUpSearchBar()
        currentCityList = cityList
    }
    
    // MARK: - SetUp View
    func setUpTableView() {
        tableViewSearch.dataSource = self
        tableViewSearch.delegate = self
        tableViewSearch.reloadData()
    }

    func setUpSearchBar() {
        searchBar.delegate = self
    }
    
    @IBAction func ActionGetGPS(_ sender: Any) {
        idReturn = 1581129
        currentReadAPIData()
        forecastReadAPIData()
    }
    
    
    func timeStringFromUnixTime1(unixTime: Double) -> String {
        let date = NSDate(timeIntervalSince1970: unixTime)
        
        //Date+Time formatting
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.full
        dateFormatter.timeStyle = DateFormatter.Style.short
        let now = dateFormatter.string(from: date as Date)
        return now
    }
    
    func timeStringFromUnixTime2(unixTime: Double) -> String {
        let date = NSDate(timeIntervalSince1970: unixTime)
        
        //Date formatting
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.short
        let now = dateFormatter.string(from: date as Date)
        return now
    }
    
    func timeStringFromUnixTime3(unixTime: Double) -> String {
        let date = NSDate(timeIntervalSince1970: unixTime)
        
        //Time formatting
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.short
        let now = dateFormatter.string(from: date as Date)
        return now
    }
    
    func currentReadAPIData() {
        NetworkManager.shared.getWeatherAPIDecoder1(idReturn) { (response) in
            if let weather = response {
                DispatchQueue.main.async {
                    self.currentPlace.text = weather.name
                    self.currentDT.text = self.timeStringFromUnixTime1(unixTime: Double(weather.dt!))
                    self.currentTemp.text = "Nhiệt độ: " + String((weather.main?.temp!)!) + "◦C"
                    self.descriptionWeather.text = (weather.weather![0].description)!
                    self.temp_Max.text = "Nhiệt độ tối đa: " + String((weather.main?.temp_max!)!) + "◦C"
                    self.temp_Min.text = "Nhiệt độ tối thiểu: " + String((weather.main?.temp_min!)!) + "◦C"
                    self.pressure.text = "Áp suất: " + String((weather.main?.pressure!)!) + " mb"
                    self.humidity.text = "Độ ẩm: " + String((weather.main?.humidity!)!) + "%"
                    self.sunrise.text = "Mặt trời mọc: " + self.timeStringFromUnixTime2(unixTime: Double((weather.sys?.sunrise!)!))
                    self.sunset.text = "Mặt trời mọc: " + self.timeStringFromUnixTime2(unixTime: Double((weather.sys?.sunset!
                        )!))
                    let urlString = "http://openweathermap.org/img/w/" + weather.weather![0].icon! + ".png"
                    let urlImage = URL(string: urlString)
                    let data = try? Data(contentsOf: urlImage!)
                    self.iconWeather.image = UIImage(data: data!)
                }
            }
            else {
                print("da xay ra loi current")
            }
        }
    }
    
    func forecastReadAPIData() {
        NetworkManager.shared.getWeatherAPIDecoder2(idReturn) {[weak self] (response) in
            if let list = response?.list {
                self?.arrList = list
                DispatchQueue.main.async {
                    self?.tableView5days.reloadData()
//                    self?.tableViewSearch.reloadData()
                }
            }
            else {
                print("da xay ra loi forecast")
            }
        }
    }
    
    func tableViewInit() {
        tableView5days.dataSource = self
        tableView5days.delegate = self
        tableView5days.register(UINib(nibName: "TableViewCellManager", bundle: nil), forCellReuseIdentifier: "Cell1")
        tableView5days.reloadData()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension CurrentWeather: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableView5days {
            return arrList.count
        }
        else if tableView == tableViewSearch {
            return currentCityList.count
        }
        else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tableView5days {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as? TableViewCellManager else {
                return UITableViewCell()
            }
            let b = timeStringFromUnixTime2(unixTime: Double(arrList[indexPath.row].dt!))
            let c = timeStringFromUnixTime3(unixTime: Double(arrList[indexPath.row].dt!))
            let d = String((arrList[indexPath.row].main?.temp)!) + " độ C"
            let urlString = "http://openweathermap.org/img/w/" + arrList[indexPath.row].weather![0].icon! + ".png"
            let urlImage = URL(string: urlString)
            let data = try? Data(contentsOf: urlImage!)
            let a = UIImage(data: data!)
            cell.setData(a, b, c, d)
            return cell
        }
        else if tableView == tableViewSearch {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") else {
                return UITableViewCell()
            }
            cell.textLabel?.text = currentCityList[indexPath.row].name
            return cell
        }
        else {
            return UITableViewCell()
        }
    }

}



extension CurrentWeather: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.tag == 2 {
            idReturn = currentCityList[indexPath.row].id!
            currentReadAPIData()
            forecastReadAPIData()
            tableViewSearch.isHidden = true
            searchBar.text = nil
        }
    }
}

extension CurrentWeather: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            tableViewSearch.isHidden = false
        }
        else {
            currentCityList = cityList
            tableViewSearch.reloadData()
            tableViewSearch.isHidden = true
            return
        }
        currentCityList = cityList.filter({ city -> Bool in
            (city.name?.lowercased().contains(searchText.lowercased()))!
        })
        tableViewSearch.reloadData()
    }
}
