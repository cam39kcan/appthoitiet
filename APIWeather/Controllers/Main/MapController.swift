//
//  Test.swift
//  AppWeather
//
//  Created by Đỗ Hồng Quân on 24/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import UIKit
import MapKit

class CustomPin: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(_ pinTitle: String, _ pinSubTitle: String, _ location: CLLocationCoordinate2D) {
        self.title = pinTitle
        self.subtitle = pinSubTitle
        self.coordinate = location
    }
}


class MapController: UIViewController {
    
    @IBOutlet weak var tableViewSearch: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: MKMapView!
    
    
    var cityList = [CityList]()
    var currentCityList = [CityList]()
    var coordinate: Coord?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ReadJSON.shared.loadData(&cityList)
        tableViewSearch.isHidden = true
        setUpTableView()
        setUpSearchBar()
        currentCityList = cityList
        // Do any additional setup after loading the view.
    }
    
    func setUpTableView() {
        tableViewSearch.dataSource = self
        tableViewSearch.delegate = self
        tableViewSearch.reloadData()
    }
    
    func setUpSearchBar() {
        searchBar.delegate = self
    }
}

extension MapController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            tableViewSearch.isHidden = false
        }
        else {
            currentCityList = cityList
            tableViewSearch.reloadData()
            tableViewSearch.isHidden = true
            return
        }
        currentCityList = cityList.filter({ city -> Bool in
            (city.name?.lowercased().contains(searchText.lowercased()))!
        })
        tableViewSearch.reloadData()
    }
}

extension MapController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentCityList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") else {
            return UITableViewCell()
        }
        cell.textLabel?.text = currentCityList[indexPath.row].name
        return cell
    }
}

extension MapController: UITableViewDelegate, MKMapViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lat = currentCityList[indexPath.row].coord?.lat
        let lon = currentCityList[indexPath.row].coord?.lon
        let id = currentCityList[indexPath.row].id
        var temp: Double?
        var description: String?
        NetworkManager.shared.getWeatherAPIDecoder1(id!) { (response) in
            if let weather = response {
                temp = weather.main?.temp
                description = weather.weather![0].description
            }
            else {
                print("Error")
            }
        }
        // Ignoring user
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        // Activity Indicator
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        
        self.view.addSubview(activityIndicator)
        
        // Hide search bar
//        searchBar.resignFirstResponder()
//        dismiss(animated: true, completion: nil)
        
        // Create the search request
        let searchRequest = MKLocalSearchRequest()
        searchRequest.naturalLanguageQuery = searchBar.text
        
        let activeSearch = MKLocalSearch(request: searchRequest)
        
        activeSearch.start { (response, error) in
            
            activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            
            if response == nil {
                print("ERROR")
            }
            else {
                // remove annotations
                //                let annotations = self.myMapView.annotations
                //                self.myMapView.removeAnnotations(annotations)
                
                // Getting data
                let latitude = lat
                let longtitude = lon
                
                // Create annotation
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2DMake(latitude!, longtitude!)
                //                print("\(latitude!) \(longtitude!)")
                self.mapView.addAnnotation(annotation)
                
                // Zooming in on annotation
                let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude!, longtitude!)
                let span = MKCoordinateSpanMake(2, 2)
                let region = MKCoordinateRegionMake(coordinate, span)
                self.mapView.setRegion(region, animated: true)
                let pin = CustomPin(String(temp!) + " độ C", description!, coordinate)
                self.mapView.addAnnotation(pin)
                self.mapView.delegate = self
                
                
                
            }
        }
        tableViewSearch.isHidden = true
        searchBar.text = nil
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        
        let annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "customannotation")
        annotationView.image = UIImage(named: "Mark")
        annotationView.canShowCallout = true
        return annotationView
    }
}
