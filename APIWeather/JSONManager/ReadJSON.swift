//
//  ReadJSON.swift
//  AppWeather
//
//  Created by Đỗ Hồng Quân on 24/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit

class ReadJSON {
    static let shared = ReadJSON()
    func loadData(_ cityList: inout [CityList]){
        
        if let path = Bundle.main.url(forResource: "city", withExtension: "json") {
            
            do {
                let jsonData = try Data(contentsOf: path, options: .mappedIfSafe)
                do {
                    if let jsonResult = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions(rawValue: 0)) as? NSArray {
                        //  if let personArray = jsonResult {
                        for (_, element) in jsonResult.enumerated() {
                            if let element = element as? NSDictionary {
                                let id = element.value(forKey: "id") as!  Int
                                let name = element.value(forKey: "name") as! String
                                let country = element.value(forKey: "country") as! String
                                
                                
                                if let coord = element.value(forKey: "coord") as? NSDictionary {
                                    //print(coord.count)
                                    //                                    for ing in coord {
                                    
                                    // if let inG = ing as? NSDictionary {
                                    //                                                print("khong loi")
                                    let lon = coord.value(forKey: "lon") as! Double
                                    let lat = coord.value(forKey: "lat") as! Double
                                    let coorddd = Coord(lon,lat)
                                    let city = CityList.init(id, name, country, coorddd)
                                    //                                                print(city.coord.lon)
                                    cityList.append(city)
                                    //   }
                                    //                                        }
                                }
                                
                                
                                
                            }
                        }
                        // }
                    }
                } catch let error as NSError {
                    print("Error: \(error)")
                }
            } catch let error as NSError {
                print("Error: \(error)")
            }
        }
    }
}
