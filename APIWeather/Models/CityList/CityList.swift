//
//  CityList.swift
//  APIWeather
//
//  Created by Đỗ Hồng Quân on 20/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit

class CityList {
    var id: Int?
    var name: String?
    var country: String?
    var coord: Coord?
    
    init(_ id: Int, _ name: String, _ country: String, _ coord: Coord) {
        self.id = id
        self.name = name
        self.country = country
        self.coord = coord
    }
}
