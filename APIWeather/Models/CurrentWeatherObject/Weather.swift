//
//  Weather.swift
//  APIWeather
//
//  Created by Đỗ Hồng Quân on 14/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit

struct Weather: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var icon: String?
}
