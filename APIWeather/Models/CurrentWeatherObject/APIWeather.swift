//
//  APIWeather.swift
//  APIWeather
//
//  Created by Đỗ Hồng Quân on 14/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit

class APIWeather: Codable {
    var base: String?
    var visibility: Int?
    var dt: Int?
    var id: Int?
    var name: String?
    var cod: Int?
    var coord: Coord?
    var weather: [Weather]?
    var main: Main?
    var clouds: Clouds?
    var sys: Sys?
}
