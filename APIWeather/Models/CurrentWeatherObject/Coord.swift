//
//  coord.swift
//  APIWeather
//
//  Created by Đỗ Hồng Quân on 14/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit

struct Coord: Codable {
    var lat: Double?
    var lon: Double?
    
    init(_ lon: Double?, _ lat: Double?) {
        self.lon = lon
        self.lat = lat
    }
}
