//
//  Main.swift
//  APIWeather
//
//  Created by Đỗ Hồng Quân on 14/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit

struct Main: Codable {
    var temp: Double?
    var pressure: Double?
    var humidity: Int?
    var temp_min: Double?
    var temp_max: Double?
    var sea_level: Double?
    var grnd_level: Double?
    var temp_kf: Double?
}
