//
//  APIWeather5days.swift
//  AppWeather
//
//  Created by Đỗ Hồng Quân on 24/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit

class APIWeather5days: Codable {
    var cod: String?
    var message: Double?
    var cnt: Int?
    var list: [List]?
    var city: City?
    
    init() {
    }
}
