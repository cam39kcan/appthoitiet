//
//  List.swift
//  AppWeather
//
//  Created by Đỗ Hồng Quân on 24/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit

struct List: Codable {
    var dt: Int?
    var main: Main?
    var weather: [Weather]?
    var clouds: Clouds?
    var wind: Wind?
    var sys: System?
    var dt_txt: String?
}
