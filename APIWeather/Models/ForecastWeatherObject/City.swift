//
//  City.swift
//  AppWeather
//
//  Created by Đỗ Hồng Quân on 24/09/2018.
//  Copyright © 2018 Đỗ Hồng Quân. All rights reserved.
//

import Foundation
import UIKit

struct City: Codable {
    var id: Int?
    var name: String?
    var coord: Coord?
    var country: String?
}
